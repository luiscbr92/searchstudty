package main;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author luicent, iredeol
 */
public class Main {

  public static ArrayList<Integer> generateRandomVector(int size) {
    if (size > 10000000) {
      return null;
    }

    ArrayList<Integer> vector = new ArrayList();
    int index;

    for (index = 0; index < size; index++) {
      vector.add((int) (Math.random() * size));
    }

    return vector;
  }
  
  public static ArrayList<Integer> copy(ArrayList<Integer> vector){
    ArrayList<Integer> copy = new ArrayList();
    
    for(int i = 0; i < vector.size(); i++){
      copy.add(vector.get(i));
    }
    
    return copy;
  }

  public static ArrayList<Integer> insertionSort(ArrayList<Integer> vector) {
    int i, j, temp;

    for (i = 1; i < vector.size(); i++) {
      j = i - 1;
      temp = vector.get(i);
      while (j >= 0 && temp < vector.get(j)) {
        vector.set(j + 1, vector.get(j));
        j--;
      }
      vector.set(j + 1, temp);
    }

    return vector;
  }

  public static void printVector(ArrayList<Integer> vector) {
    System.out.print("[");
    for (int i = 0; i < vector.size(); i++) {
      if (i != 0) {
        System.out.print(", ");
      }
      System.out.print(vector.get(i));
    }
    System.out.println("]");
  }

  public static Pair<Long, Long> sequentialSearch(int target, ArrayList<Integer> vector) {
    int index = 0;
    long comparisons;
    comparisons = 0;
    long time_start = System.nanoTime();

    while (index < vector.size() && target != vector.get(index)) {
      index++;
      comparisons++;
    }

    long time_required = System.nanoTime() - time_start;

    if (index < vector.size()) {
      comparisons++;
    }

    return new Pair(comparisons, time_required);
  }

  public static Pair<Long, Long> binarySearch(int target, ArrayList<Integer> vector) {
    int leftIndex, rightIndex, midIndex;
    leftIndex = 0;
    long comparisons;
    comparisons = 0;
    rightIndex = vector.size() - 1;
    long time_start = System.nanoTime();

    while (leftIndex <= rightIndex) {
      midIndex = (leftIndex + rightIndex) / 2;
      // Cosider only one comparison
      comparisons++;
      if (target < vector.get(midIndex)) {
        rightIndex = midIndex - 1;
      } else if (target > vector.get(midIndex)) {
        leftIndex = midIndex + 1;
      } else {
        break;
      }
    }

    long time_required = System.nanoTime() - time_start;

    return new Pair(comparisons, time_required);
  }

  public static Pair<Long, Long> binarySearchWithSorting(int target, ArrayList<Integer> vector) {
    int leftIndex, rightIndex, midIndex;
    leftIndex = 0;
    long comparisons;
    comparisons = 0;
    rightIndex = vector.size() - 1;
    int i, j, temp;
    long time_start = System.nanoTime();

    for (i = 1; i < vector.size(); i++) {
      j = i - 1;
      temp = vector.get(i);
      while (j >= 0 && temp < vector.get(j)) {
        comparisons++;
        vector.set(j + 1, vector.get(j));
        j--;
      }
      if (j >= 0) {
        comparisons++;
      }
      vector.set(j + 1, temp);
    }

    while (leftIndex <= rightIndex) {
      midIndex = (leftIndex + rightIndex) / 2;
      // Cosider only one comparison
      comparisons++;
      if (target < vector.get(midIndex)) {
        rightIndex = midIndex - 1;
      } else if (target > vector.get(midIndex)) {
        leftIndex = midIndex + 1;
      } else {
        break;
      }
    }

    long time_required = System.nanoTime() - time_start;

    return new Pair(comparisons, time_required);
  }

  public static void main(String[] args) {

    WaitWindow frame = new WaitWindow();
    frame.setVisible(true);

    String bufferSorted, bufferSortedAvg, bufferUnsorted, bufferUnsortedAvg;
    bufferSorted = "Size;Op_Seq;Time_Seq;Op_Bin;Time_Bin;\n";
    bufferSortedAvg = "Size;Op_Seq;Time_Seq;Op_Bin;Time_Bin;\n";
    bufferUnsorted = "Size;Op_Seq;Time_Seq;Op_Bin;Time_Bin;\n";
    bufferUnsortedAvg = "Size;Op_Seq;Time_Seq;Op_Bin;Time_Bin;\n";

    int size, rep, rand;
    double seqTimeSorted, binTimeSorted, seqTimeUnsorted, binTimeUnsorted, seqOperationsSorted, binOperationsSorted, seqOperationsUnsorted, binOperationsUnsorted;
    Pair<Long, Long> seqResultsSorted, binResultsSorted, seqResultsUnsorted, binResultsUnsorted;

    int totalIterations = 30;
    boolean spanishDecimal = true; // If true, uses comma for decimals in the output

    ArrayList<Integer> vector, sortedVector;
    for (size = 10; size <= 100000; size *= 10) {
      seqOperationsSorted = 0;
      seqOperationsUnsorted = 0;
      seqTimeSorted = 0;
      seqTimeUnsorted = 0;
      binOperationsSorted = 0;
      binOperationsUnsorted = 0;
      binTimeSorted = 0;
      binTimeUnsorted = 0;
      for (rep = 0; rep < totalIterations; rep++) {
        vector = generateRandomVector(size);
        sortedVector = copy(vector);
        sortedVector = insertionSort(sortedVector);

        rand = (int) (Math.random() * size);

        seqResultsSorted = sequentialSearch(rand, sortedVector);
        binResultsSorted = binarySearch(rand, sortedVector);
        seqResultsUnsorted = sequentialSearch(rand, vector);
        binResultsUnsorted = binarySearchWithSorting(rand, vector);

        seqOperationsSorted += seqResultsSorted.getLeft();
        seqTimeSorted += seqResultsSorted.getRight();
        seqOperationsUnsorted += seqResultsUnsorted.getLeft();
        seqTimeUnsorted += seqResultsUnsorted.getRight();

        binOperationsSorted += binResultsSorted.getLeft();
        binTimeSorted += binResultsSorted.getRight();
        binOperationsUnsorted += binResultsUnsorted.getLeft();
        binTimeUnsorted += binResultsUnsorted.getRight();

        bufferSorted += Integer.toString(size) + ';'
                + Long.toString(seqResultsSorted.getLeft()) + ';'
                + Long.toString(seqResultsSorted.getRight()) + ';'
                + Long.toString(binResultsSorted.getLeft()) + ';'
                + Long.toString(binResultsSorted.getRight()) + ';' + '\n';
        bufferUnsorted += Integer.toString(size) + ';'
                + Long.toString(seqResultsUnsorted.getLeft()) + ';'
                + Long.toString(seqResultsUnsorted.getRight()) + ';'
                + Long.toString(binResultsUnsorted.getLeft()) + ';'
                + Long.toString(binResultsUnsorted.getRight()) + ';' + '\n';
      }

      seqOperationsSorted /= totalIterations;
      seqTimeSorted /= totalIterations;
      seqOperationsUnsorted /= totalIterations;
      seqTimeUnsorted /= totalIterations;

      binOperationsSorted /= totalIterations;
      binTimeSorted /= totalIterations;
      binOperationsUnsorted /= totalIterations;
      binTimeUnsorted /= totalIterations;

      if (spanishDecimal) {
        bufferSortedAvg += Integer.toString(size) + ';'
                + Double.toString(seqOperationsSorted).replace('.', ',') + ';'
                + Double.toString(seqTimeSorted).replace('.', ',') + ';'
                + Double.toString(binOperationsSorted).replace('.', ',') + ';'
                + Double.toString(binTimeSorted).replace('.', ',') + ';' + '\n';
        bufferUnsortedAvg += Integer.toString(size) + ';'
                + Double.toString(seqOperationsUnsorted).replace('.', ',') + ';'
                + Double.toString(seqTimeUnsorted).replace('.', ',') + ';'
                + Double.toString(binOperationsUnsorted).replace('.', ',') + ';'
                + Double.toString(binTimeUnsorted).replace('.', ',') + ';' + '\n';

      } else {
        bufferSortedAvg += Integer.toString(size) + ';'
                + Double.toString(seqOperationsSorted) + ';'
                + Double.toString(seqTimeSorted) + ';'
                + Double.toString(binOperationsSorted) + ';'
                + Double.toString(binTimeSorted) + ';' + '\n';
        bufferUnsortedAvg += Integer.toString(size) + ';'
                + Double.toString(seqOperationsUnsorted) + ';'
                + Double.toString(seqTimeUnsorted) + ';'
                + Double.toString(binOperationsUnsorted) + ';'
                + Double.toString(binTimeUnsorted) + ';' + '\n';
      }
    }

    FileWriter fileSorted, fileSortedAvg, fileUnsorted, fileUnsortedAvg;
    PrintWriter pwSorted, pwSortedAvg, pwUnsorted, pwUnsortedAvg;
    try {
      fileSorted = new FileWriter("VectorOrdenado.csv");
      pwSorted = new PrintWriter(fileSorted);

      fileSortedAvg = new FileWriter("VectorOrdenadoConPromedio.csv");
      pwSortedAvg = new PrintWriter(fileSortedAvg);

      fileUnsorted = new FileWriter("VectorNoOrdenado.csv");
      pwUnsorted = new PrintWriter(fileUnsorted);

      fileUnsortedAvg = new FileWriter("VectorNoOrdenadoConPromedio.csv");
      pwUnsortedAvg = new PrintWriter(fileUnsortedAvg);

      pwSorted.print(bufferSorted);
      pwSortedAvg.print(bufferSortedAvg);
      pwUnsorted.print(bufferUnsorted);
      pwUnsortedAvg.print(bufferUnsortedAvg);

      fileSorted.close();
      fileSortedAvg.close();
      fileUnsorted.close();
      fileUnsortedAvg.close();

    } catch (Exception e) {
    }
    
    frame.dispose();

    System.exit(0);
  }
}
