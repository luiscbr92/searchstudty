package main;

/**
 *
 * @author luicent, iredeol
 * @param <T>
 * @param <E>
 */
public class Pair<T,E> {
  private T left;
  private E right;

  public Pair(T left, E right) {
    this.left = left;
    this.right = right;
  }

  public T getLeft() {
    return left;
  }

  public E getRight() {
    return right;
  }

  public void setLeft(T left) {
    this.left = left;
  }

  public void setRight(E right) {
    this.right = right;
  }
  
  
}
